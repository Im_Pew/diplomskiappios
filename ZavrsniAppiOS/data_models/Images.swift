//
//  Images.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 12/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

struct Images{
    var photo: String
    var typeIcon: String
    var weaknessIcon: String
    
    static var BASEURL = "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/"
}
