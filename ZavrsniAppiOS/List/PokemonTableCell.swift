//
//  PokemonTableCell.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class PokemonTableCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    private var _pokemonList: PokemonList? = nil
    private var contract: ListContract? = nil
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func prepare(pokemonList: PokemonList, contract: ListContract) {
        
        cellLabel.textColor = UIColor.init(red: 0, green: 255, blue: 0, alpha: 255)
        
        self.contract = contract
        self._pokemonList = pokemonList
        
        cellLabel.text = pokemonList.name
        
        if let url = URL(string: pokemonList.url) {
            cellImage.kf.setImage(with: url)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.someAction(_:)))
        
        self.addGestureRecognizer(tapGesture)
    }

    @objc func someAction(_ sender:UITapGestureRecognizer){
        contract?.handleEvent(event: 100, value: self._pokemonList?.name ?? "")
    }
}
