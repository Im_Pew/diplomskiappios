//
//  DataProvider.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DataProvider {
    
    static var SPRITES_URL = "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/sprites/[pok].png";
    
    static func getSpriteUrl(name: String) -> String {
        return SPRITES_URL.replacingOccurrences(of: "[pok]", with: name)
    }

    static func getPokemons(completion: @escaping ([PokemonList]?) -> Void, error: @escaping (String?) -> Void) {
        
        var list: [PokemonList] = []
        
        AF.request("https://pokeapi.co/api/v2/pokemon?limit=151", encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                for pokemon in json["results"].arrayValue {
                    
                    let stringId = pokemon["url"].stringValue.split(separator: "/").last
                    let id = String(stringId ?? "0")
                
                    let stringName = pokemon["name"].stringValue
                    
                    let pokeObject = PokemonList(id: Int(id)!, name: stringName, codeName: stringName, url: getSpriteUrl(name: stringName))
                    
                    list.append(pokeObject)
                }
                
                completion(list)
                
            case .failure(let errorMsg):
                print(errorMsg)
                error(errorMsg.errorDescription ?? "")
            }
        }
    }
    
    static func getPokemon(name: String, completion: @escaping (Pokemon?) -> Void, error: @escaping (String?) -> Void) {
        print("https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/pokedex.php?pokemon=" + name)
        AF.request("https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/pokedex.php?pokemon=" + name, encoding: JSONEncoding.default).responseJSON {response in
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                let infoJson = json["info"]
                let imagesJson = json["images"]
                
                let images = Images(
                    photo: Images.BASEURL + imagesJson["photo"].stringValue,
                    typeIcon: Images.BASEURL + imagesJson["typeIcon"].stringValue,
                    weaknessIcon: Images.BASEURL + imagesJson["weaknessIcon"].stringValue)
                
                let info = Info(
                    id: infoJson["id"].intValue,
                    type: infoJson["type"].stringValue,
                    weakness: infoJson["weakness"].stringValue,
                    description: infoJson["description"].stringValue
                )
                
                let pokemon = Pokemon(
                    name: json["name"].stringValue,
                    hp: json["hp"].intValue,
                    info: info,
                    images: images
                )
                
                completion(pokemon)
                
            case .failure(let errorMsg):
                print(errorMsg)
                error(errorMsg.errorDescription ?? "")
            }
        }
    }
    
}
