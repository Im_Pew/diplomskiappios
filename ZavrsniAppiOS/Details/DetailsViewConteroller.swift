//
//  DetailsViewConteroller.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 12/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet var localView: DetailsView!
    private var localModel: DetailsModel? = nil
    var _pokemon: Pokemon? = nil
    
    func prepare(pokemon: Pokemon) {
        self._pokemon = pokemon
        localModel = DetailsModel()
    }
    
    override func viewDidLoad() {
        if let pokemon = _pokemon, let _model = localModel {
            if (localView != nil) {
                localView.prepare(pokemon: pokemon, model: _model)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let name = _pokemon?.name {
            prepareTitle( titleText: name)
        }
    }
    
    private func prepareTitle(titleText: String) {
        title = titleText
        
        self.navigationController?.navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: UIColor.white,
         NSAttributedString.Key.font: UIFont(name: "Retro Computer", size: 21)!]

        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 204, green: 0, blue: 0, alpha: 255)
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
    }
}
