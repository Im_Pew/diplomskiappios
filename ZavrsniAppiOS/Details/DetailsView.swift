//
//  DetailsView.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 12/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import UIKit

class DetailsView: UIView {
    private var _pokemon: Pokemon? = nil
    private var localModel: DetailsModel? = nil
    
    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var typeIcon: UIImageView!
    @IBOutlet weak var weaknessIcon: UIImageView!
    @IBOutlet weak var pokemonName: UILabel!
    @IBOutlet weak var pokemonHp: UILabel!
    @IBOutlet weak var pokemonDescription: UITextView!
    
    func prepare(pokemon: Pokemon, model: DetailsModel) {
        self._pokemon = pokemon
        self.localModel = model
        
        if let imageUrl = URL(string: (_pokemon?.images.photo)!) {
            pokemonImage.kf.setImage(with: imageUrl)
        }
        
        if let typeIconUrl = URL(string: pokemon.images.typeIcon) {
            typeIcon.kf.setImage(with: typeIconUrl)
        }
        
        if let weaknessIconUrl = URL(string: pokemon.images.weaknessIcon) {
            weaknessIcon.kf.setImage(with: weaknessIconUrl)
        }
        
        pokemonHp.text = String(pokemon.hp)
        pokemonName.text = pokemon.name
        pokemonDescription.text = pokemon.info.description
        
        let size = pokemonDescription.frame
//        pokemonDescription.translatesAutoresizingMaskIntoConstraints = true
//        pokemonDescription.sizeToFit()
        pokemonDescription.isScrollEnabled = false
        
//        pokemonDescription.frame = CGRect(x: 0, y: 0, width: size.width, height: pokemonDescription.frame.height)

    }
 
}
