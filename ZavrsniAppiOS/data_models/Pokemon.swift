//
//  Pokemon.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 12/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation

struct Pokemon {
    let name: String
    let hp: Int
    let info: Info
    let images: Images

}
