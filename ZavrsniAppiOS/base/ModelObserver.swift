//
//  ModelObserver.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

class ModelObserver: ModelProtocol, Equatable {
    
    static func == (lhs: ModelObserver, rhs: ModelObserver) -> Bool {
        return lhs == rhs;
    }
    
    
    func update(flag: Int) {
        
    }
}
