//
//  AsyncResponseProtocol.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation

protocol AsyncResponseProtocol {
    associatedtype T
    
    func result(data: T)
    func error(error: String)
}
