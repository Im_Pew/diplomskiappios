//
//  BaseModel.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation

open class BaseModel {
    
    var _subscribers: [ModelProtocol] = []
    
    func subscribe(modelProtocol: ModelProtocol) {
        _subscribers.append(modelProtocol);
    }
    
    func unsubscribe(modelProtocol: ModelProtocol) {
        _subscribers = _subscribers.filter ({$0.id != modelProtocol.id});
    }
    
    func notify(flag: Int) {
        for subscriber in _subscribers {
            subscriber.update(flag: flag);
        }
    }
}
