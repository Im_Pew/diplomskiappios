//
//  ModelProtocol.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation

protocol ModelProtocol {
    var id : String { get }
    func update(flag: Int)
}
