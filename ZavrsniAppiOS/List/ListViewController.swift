//
//  ListViewController.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, ModelProtocol, ListContract, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    
    static let GET_POKEMON = 100;
    static let REFRESH = 102;
    
    var id: String = "ListViewController"
    
    @IBOutlet var localView: ListView!
    var localModel: ListModel? = nil
    var searchController : UISearchController!
    
    @IBAction func searchButton(_ sender: UIBarButtonItem) {
        
//        searchController.hidesNavigationBarDuringPresentation = true
//        searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable

//        self.searchController.searchBar.delegate = self
//        present(searchController, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localModel = ListModel()
        localView.prepare(model: localModel!, contract: self)
        localModel?.subscribe(modelProtocol: self)
        localModel?.fetchPokemons()
        
        searchController = UISearchController(searchResultsController: nil)
//        searchController.hidesNavigationBarDuringPresentation = true
        searchController.obscuresBackgroundDuringPresentation = false
//        searchController.automaticallyShowsScopeBar = true
//                searchController.searchBar.keyboardType = UIKeyboardType.asciiCapable
        searchController.searchResultsUpdater = self
//        searchController.searchBar.delegate = self
        
//        navigationItem.hidesSearchBarWhenScrolling = true
        navigationItem.searchController = searchController
//        definesPresentationContext = true
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        localView.applyFilter(filter: searchController.searchBar.text ?? "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareTitle(titleText: "Pokedex")
    }
    
    func handleEvent(event: Int, value: String) {
        if (event == ListViewController.GET_POKEMON) {
            self.localModel?.fetchPokemon(name: value)
            showLoader()
        } else if (event == ListViewController.REFRESH) {
            self.localModel?.fetchPokemons()
            showLoader()
        }
    }
    
    func update(flag: Int) {
        if (flag == 101) {
            let pok = localModel?._pokemon
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
            
            self.navigationController?.show(vc, sender: nil)
            vc.prepare(pokemon: pok!)
            
            showLoader()
        }
        if (coverView != nil) {
            showLoader()
        }
    }
    
    private func prepareTitle(titleText: String) {
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        
        title = titleText
        
        self.navigationController?.navigationBar.titleTextAttributes =
        [NSAttributedString.Key.foregroundColor: UIColor.systemGreen,
         NSAttributedString.Key.font: UIFont(name: "Retro Computer", size: 21)!]
    }
    
    private var coverView: UIView? = nil
    
    private func showLoader() {
        if (coverView == nil) {
            let screenRect = UIScreen.main.bounds
            coverView = UIView(frame: screenRect)
            coverView?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
            let loadingIndicator = UIActivityIndicatorView(style: .large)
            loadingIndicator.color = UIColor.white
            loadingIndicator.frame = CGRect(x: self.view.frame.width / 2 - 36, y: self.view.frame.height / 2 - 36, width: 72, height: 72)
        
            loadingIndicator.startAnimating()
            coverView!.addSubview(loadingIndicator)
        
            self.view.addSubview(coverView!)
        } else {
            coverView?.removeFromSuperview()
            coverView = nil
        }
    }
}

protocol ListContract{
    func handleEvent(event: Int, value: String)
}
