//
//  Info.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 12/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

struct Info {
    var id: Int
    var type: String
    var weakness: String
    var description: String
}
