//
//  ListModel.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation

class ListModel: BaseModel {
    
    public static let ERROR = 1;
    public static let POKEMONS = 100;
    public static let POKEMON = 101;
    
    var _pokemons: [PokemonList] = []
    var _pokemon: Pokemon? = nil
    
    var _errorMsg = ""
    
    func fetchPokemons() {
        
        let pokemons = DataProvider.getPokemons(completion: {data in
            self._pokemons = data ?? []
            self.notify(flag: ListModel.POKEMONS)
        }, error: {error in
            self._errorMsg = error ?? ""
            self.notify(flag: ListModel.ERROR)
        })
    }
    
    func fetchPokemon(name: String) {
        DataProvider.getPokemon(name: name, completion: {pokemon in
            self._pokemon = pokemon
            self.notify(flag: ListModel.POKEMON)
        }, error: {error in
            self._errorMsg = error ?? ""
            self.notify(flag: ListModel.ERROR)
        })
    }
}
