//
//  PokemonList.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PokemonList {
    var id: Int
    var name: String
    var codeName: String
    var url: String
}
