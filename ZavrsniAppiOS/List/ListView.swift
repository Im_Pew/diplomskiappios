//
//  ListView.swift
//  ZavrsniAppiOS
//
//  Created by Danijel Turić on 11/08/2020.
//  Copyright © 2020 Danijel Turić. All rights reserved.
//

import TTGSnackbar
import  UIKit

class ListView: UIView, ModelProtocol, UITableViewDelegate, UITableViewDataSource {
    
    var id = "ListView"
    private var model: ListModel? = nil
    private var contract: ListContract? = nil
    private var refreshControl = UIRefreshControl()
    
    private var _pokemonData: [PokemonList] = []
    private var _filteredPokemonData: [PokemonList] = []
    
    @IBOutlet weak var listView: UITableView!
    
    func prepare(model: ListModel, contract: ListContract) {
        self.model = model
        self.contract = contract
        
        model.subscribe(modelProtocol: self)
    }
    
    func update(flag: Int) {
        if (flag == ListModel.POKEMONS) {
            self._pokemonData = model?._pokemons ?? []
            self._filteredPokemonData = self._pokemonData
            
            listView.delegate = self
            listView.dataSource = self
            listView.reload()
        } else if (flag == ListModel.ERROR) {
            TTGSnackbar(message: model?._errorMsg ?? "", duration: .middle).show()
        }
    }
    
    func applyFilter(filter: String) {
        
        if (filter == "") {
            self._filteredPokemonData = self._pokemonData
        } else {
            _filteredPokemonData = _pokemonData.filter ({$0.name.lowercased().contains(filter.lowercased() )});
        }
    
        listView.reloadData()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        listView.addRefreshControl(target: self, selector: #selector(_handleRefresh(sender:)))
    }
    
    override func layerWillDraw(_ layer: CALayer) {
        listView.delegate = self
        listView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _filteredPokemonData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonTableCell", for: indexPath) as! PokemonTableCell
        
        cell.prepare(pokemonList: _filteredPokemonData[indexPath.row], contract: contract!)
        
        return cell
    }
    
//    @objc func refresh(refreshControl: UIRefreshControl) {
//        print("Ready to refresh!")
//        contract?.handleEvent(event: ListViewController.REFRESH, value: "refresh");
//    }
    
    @IBAction private func _handleRefresh(sender: UIRefreshControl) {
        sender.beginRefreshing()
        contract?.handleEvent(event: ListViewController.REFRESH, value: "refresh");
    }
}

extension UITableView {
    func addRefreshControl(target: Any?, selector: Selector) {
        let refresher = UIRefreshControl()
        refresher.addTarget(target, action: selector, for: .valueChanged)
        refresher.tintColor = .white
        refresher.bounds = CGRect(x: refresher.bounds.origin.x,
               y: self.contentInset.top,
        width: refresher.bounds.size.width,
        height: refresher.bounds.size.height)
        self.refreshControl = refresher
    }
    func reload() {
        self.refreshControl?.endRefreshing()
        self.reloadData()
    }
}
